# Test Program for PyTorch Models

# Install

Install CUDA + cuDNN + Libtorch

## CUDA

Tested with version 10.2

https://developer.nvidia.com/cuda-zone

## CuDNN

Tested with version 8.0.1

https://developer.nvidia.com/cudnn

## Libtorch

Tested with 1.5.1 and nighly build

https://pytorch.org/

# Compilation

Provide the path to which libtorch was installed (uncompressed)

```shell
    mkdir build
    cd build
    cmake -DCMAKE_PREFIX_PATH=/path/to/libtorch .. 
    make
```

See https://github.com/pytorch/pytorch/issues/40965 if you have issues with CuDNN.

# Run

```shell
    # In the build directory
    ./example-app <model> [cuda]
```

## Example CPU

```shell
    cd build
    ./exampple-app ../script-module-cpu/tcn-traced.pt
    
    # Will show
    model loaded
    5000 values from file loaded in vector
    Tensor loaded with test data
    Evaluating model : ../script-module-cpu/tcn-traced.pt
    Evaluating 1000 windows = 1445368675[ns]
    Throughput = 345933 samples per second ~ 11.5311 channels
    Inference finished
    [FAIL] is NOT close enough to expected output from LSTM FCN
    [PASS] is close enough to expected output from TCN
    [FAIL] is NOT close enough to expected output from LSTM STACK
```

Here we see the results are close enough the expected output from TCN. The model should "pass" the corresponding line and fail the other lines.

## Example GPU

```shell
    cd build
    ./example-app ../script-module-gpu/lstm-fcn-traced.pt cuda
    
    # Will show
    CUDA is available, using CUDA
    model loaded
    5000 values from file loaded in vector
    Tensor loaded with test data
    Evaluating model : ../script-module-gpu/lstm-fcn-traced.pt
    Evaluating 1000 windows = 612644042[ns]
    Throughput = 816135 samples per second ~ 27.2045 channels
    Inference finished
    [FAIL] is NOT close enough to expected output from LSTM FCN
    [FAIL] is NOT close enough to expected output from TCN
    [FAIL] is NOT close enough to expected output from LSTM STACK
```

Here we see the results are not close enough to any model. Note that the "cuda" keyword is required for GPU execution.

# Notes

Some models can be run on CPU and GPU, the "cuda" keyword can be used to choose the target. Other models can only run on CPU or GPU and will return an error if run on the wrong target. Set the keyword accordingly.
