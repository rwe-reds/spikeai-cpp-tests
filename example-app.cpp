#include <torch/script.h> // One-stop header.
#include <torch/torch.h>

#include <iostream>
#include <memory>
#include <chrono>

int main(int argc, const char* argv[]) {
    if (argc < 2) {
        std::cerr << "usage: example-app <path-to-exported-script-module> [device]\n";
        return -1;
    }

    torch::Device device("cpu");

    if (argc > 2) {
        if ((strcmp(argv[2], "cuda") == 0) and torch::cuda::is_available()) {
            std::cout << "CUDA is available, using CUDA" << std::endl;
            device = torch::Device("cuda:0");
        } else {
            std::cout << "CUDA is not available, using CPU" << std::endl;
            //device = torch::Device("cpu");
        }
    }

    torch::jit::script::Module model;
    try {
        // Deserialize the ScriptModule from a file using torch::jit::load().
        model = torch::jit::load(argv[1], device);
    }
    catch (const c10::Error& e) {
        std::cerr << "error loading the model\n";
        std::cerr << e.msg() << std::endl;
        return -1;
    }

    std::cout << "model loaded\n";


    // Read the data from file (these are actually 10 windows of 500 points serialized into a file)
    std::vector<float> data;
    std::ifstream ifile("../test_tx.csv", std::ios::in);
    if (!ifile.is_open()) {
        std::cerr << "There was a problem opening the input file!\n";
        exit(1);
    }
    double num = 0.0;
    //keep storing values from the text file so long as data exists:
    while (ifile >> num) {
        data.push_back(num);
    }
    std::cout << data.size() << " values from file loaded in vector" << std::endl;


    // Create a tensor with it with 10x 500 points (10 windows of 500 samples)
    torch::Tensor tx = torch::empty({10, 500});
    auto tx_access = tx.accessor<float, 2>();

    // Fill the tensor
    for(int i = 0; i < 10; ++i) {
        for(int j = 0; j < 500; ++j) {
            tx_access[i][j] = data.at(i*500+j);
        }
    }

    // Sanity check
    std::cout << "Tensor loaded with test data" << std::endl;
    //std::cout << "tx[0][0] = " << tx[0][0] << std::endl;
    //std::cout << "tx[0][1] = " << tx[0][1] << std::endl;
    //std::cout << "tx[1][0] = " << tx[1][0] << std::endl;

    // Create the expected output tensors (there probably is a better way to do this)
    // This data was provided by Bruno with the models
    float expected_output[3][10][5] = {
        {{ 7.6444e+00,  2.3195e+00, -2.4472e+00,  3.0354e-01, -6.5069e+00},
         { 7.7716e+00,  2.3053e+00, -2.9069e+00,  1.1159e-01, -6.0887e+00},
         { 8.1224e+00,  2.1112e+00, -3.0231e+00,  6.5920e-02, -6.3118e+00},
         { 7.5988e+00,  2.3433e+00, -2.6709e+00,  3.1133e-01, -6.2862e+00},
         { 7.2359e+00,  2.1497e+00, -2.1752e+00,  4.8202e-01, -6.1607e+00},
         { 7.5759e+00,  2.4927e+00, -2.8671e+00,  3.2492e-01, -6.2269e+00},
         { 7.9222e+00,  2.5041e+00, -2.7659e+00, -1.3972e-03, -6.5110e+00},
         { 7.6680e+00,  1.9347e+00, -2.5548e+00,  4.0402e-01, -6.2172e+00},
         { 7.6784e+00,  2.2479e+00, -2.4206e+00,  1.6377e-01, -6.3945e+00},
         { 8.0551e+00,  2.3176e+00, -3.2535e+00,  1.7483e-01, -6.2636e+00}},

        {{ 2.8700, -4.7133, -3.9628, -0.6801, -1.9677},
         { 3.4434, -4.5846, -4.2616, -0.8697, -2.1330},
         { 3.2162, -4.8013, -4.1538, -0.9034, -1.9681},
         { 3.1591, -4.3424, -4.1072, -0.7599, -2.1809},
         { 2.2962, -4.2915, -3.4174, -0.3006, -2.1322},
         { 2.7237, -4.0899, -3.7246, -0.5644, -2.2106},
         { 4.8415, -4.7048, -5.4702, -1.5847, -2.6600},
         { 1.9206, -4.1088, -3.1265, -0.2120, -2.0449},
         { 3.1806, -4.1210, -4.0725, -0.8408, -2.3120},
         { 3.3738, -4.5914, -4.3001, -0.9525, -2.1741}},

        {{ 7.2653, -1.0978, -3.0669, -3.7074, -2.9798},
         { 7.3504, -1.1748, -3.1258, -3.7371, -2.9340},
         { 7.6555, -1.3509, -3.3483, -3.7868, -2.8813},
         { 7.6688, -1.2754, -3.3711, -3.7834, -2.9306},
         { 7.3746, -1.0818, -3.1602, -3.7367, -2.9929},
         { 7.4848, -1.1893, -3.2300, -3.7523, -2.9553},
         { 7.3236, -1.1921, -3.1068, -3.7302, -2.9200},
         { 7.5245, -1.1285, -3.2695, -3.7623, -2.9937},
         { 7.2421, -0.9281, -3.1015, -3.6955, -3.0575},
         { 7.4794, -1.1707, -3.2395, -3.7458, -2.9591}}};

    // Expected output
    auto expected_output_lstm_fcn = torch::empty({10, 5});
    auto expected_output_lstm_fcn_access = expected_output_lstm_fcn.accessor<float, 2>();
    // Fill the tensor
    for(int i = 0; i < 10; ++i) {
        for(int j = 0; j < 5; ++j) {
            expected_output_lstm_fcn_access[i][j] = expected_output[0][i][j];
        }
    }
    auto expected_output_tcn = torch::empty({10, 5});
    auto expected_output_tcn_accss = expected_output_tcn.accessor<float, 2>();
    // Fill the tensor
    for(int i = 0; i < 10; ++i) {
        for(int j = 0; j < 5; ++j) {
            expected_output_tcn_accss[i][j] = expected_output[1][i][j];
        }
    }
    auto expected_output_lstm_stack = torch::empty({10, 5});
    auto expected_output_lstm_stack_access = expected_output_lstm_stack.accessor<float, 2>();
    // Fill the tensor
    for(int i = 0; i < 10; ++i) {
        for(int j = 0; j < 5; ++j) {
            expected_output_lstm_stack_access[i][j] = expected_output[2][i][j];
        }
    }

    /////////////////////
    // Evaluate Models //
    /////////////////////
    std::cout << "Evaluating model : " << argv[1] << std::endl;

    // Prepare input data
    std::vector<torch::jit::IValue> tx_ivalues;
    tx_ivalues.push_back(tx.to(device));


    // Do batch inference (for benchmarking)
    ////////////////////////////////////////
    int batchey = 100;
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    for(int i = 0; i < batchey; ++i) {
        // Inference
        auto out = model.forward(tx_ivalues).toTensor(); // 10 Windows
    }
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    auto number_windows = 10*batchey;
    auto duration_ns = std::chrono::duration_cast<std::chrono::nanoseconds> (end - begin).count();
    std::cout << "Evaluating " << number_windows << " windows = " << duration_ns << "[ns]" << std::endl;
    auto throughput = double(number_windows*500)/(duration_ns/1e9);
    std::cout << "Throughput = " << throughput << " samples per second ~ " << throughput/30e3 << " channels" << std::endl;



    // Inference for evaluation (the same as done above in the loop)
    ////////////////////////////////////////////////////////////////
    auto out = model.forward(tx_ivalues).toTensor().to(torch::Device("cpu"));
    std::cout << "Inference finished" << std::endl;

    // Evaluation against reference results
    ///////////////////////////////////////
    if (at::allclose(out, expected_output_lstm_fcn, 1e-4)) {
        std::cout << "[PASS] is close enough to expected output from LSTM FCN" << std::endl;
    } else {
        std::cout << "[FAIL] is NOT close enough to expected output from LSTM FCN" << std::endl;
    }
    if (at::allclose(out, expected_output_tcn, 3e-4)) {
        std::cout << "[PASS] is close enough to expected output from TCN" << std::endl;
    } else {
        std::cout << "[FAIL] is NOT close enough to expected output from TCN" << std::endl;
    }
    if (at::allclose(out, expected_output_lstm_stack, 1e-4)) {
        std::cout << "[PASS] is close enough to expected output from LSTM STACK" << std::endl;
    } else {
        std::cout << "[FAIL] is NOT close enough to expected output from LSTM STACK" << std::endl;
    }

    return 0;
}
